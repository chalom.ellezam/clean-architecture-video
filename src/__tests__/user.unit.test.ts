import {CreateUser} from "../usecases/CreateUser";
import {InMemoryUserRepository} from "../adapters/repositories/InMemoryUserRepository";
import {User} from "../domain/entities/User";
import {GetUserByName} from "../usecases/GetUserByName";

const inMemoryDb = new Map<string, User>()
const inMemoryUserRepository = new InMemoryUserRepository(inMemoryDb)

describe('User unit testing', () => {
    it('Should create a user', async () => {
        const createUser = await new CreateUser(inMemoryUserRepository).execute({
            name: 'Barack'
        });
        expect(createUser.name).toEqual('Barack');
    })

    it('Should create a user and get it by his name', async () => {
        await new CreateUser(inMemoryUserRepository).execute({
            name: 'Donald'
        });
        const getByName = await new GetUserByName(inMemoryUserRepository).execute({
            name: 'Donald'
        })
        expect(getByName.name).toEqual('Donald')
    })

    it('Should throw an error if user not found', async () => {
        const getByName = new GetUserByName(inMemoryUserRepository).execute({
            name: 'IAmTheNotFoundUser'
        })
        await expect(getByName).rejects.toThrow(
            Error
        )
    })

})
