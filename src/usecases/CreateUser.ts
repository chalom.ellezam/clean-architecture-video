import {Usecase} from "../domain/models/Usecase";
import {User} from "../domain/entities/User";
import {UserRepository} from "../domain/repositories/UserRepository";

interface CreateUserRequest {
    name: string;
}


export class CreateUser implements Usecase<CreateUserRequest, User> {

    constructor(
        private readonly _userRepository: UserRepository,
    ) {
    }

    async execute(request: CreateUserRequest) {
        return await this._userRepository.create(new User(request.name));
    }
}
