import {Usecase} from "../domain/models/Usecase";
import {User} from "../domain/entities/User";
import {UserRepository} from "../domain/repositories/UserRepository";

export interface GetUserByNameRequest {
    name: string;
}


export class GetUserByName implements Usecase<GetUserByNameRequest, User> {

    constructor(
        private readonly _userRepository: UserRepository
    ) {
    }

    async execute(request: GetUserByNameRequest): Promise<User> {
        return await this._userRepository.getByName(request.name);
    }
}
