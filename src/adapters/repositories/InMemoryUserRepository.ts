import {UserRepository} from "../../domain/repositories/UserRepository";
import {User} from "../../domain/entities/User";

export class InMemoryUserRepository implements UserRepository {

    constructor(
        private readonly _inMemoryDb: Map<string, User>,
    ) {
    }

    async getByName(name: string): Promise<User> {
        const user = this._inMemoryDb.get(name);
        if (!user) {
            throw new Error('user_not_found');
        }
        return user;
    }

    async create(user: User): Promise<User> {
        this._inMemoryDb.set(user.name, user);
        return user;
    }
}
